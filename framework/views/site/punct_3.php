<?if (count($result)){?>
<table class="table table-bordered">
  <thead>
    <tr>
	    <th colspan="4"></th>
      <th>ID</th>
      <th>Title</th>
      <th>Name</th>
      <th>Created</th>
      <th>Changed</th>
    </tr>
  </thead>
  <tbody>
  <?foreach ($result as $item){?>
    <tr>
      <td style="width:10px;"><?=(($item['offset']<1)?'<span class="glyphicon glyphicon-arrow-right"></span>':'')?></td>
      <td style="width:10px;"><?=(($item['offset']<2)?'<span class="glyphicon glyphicon-arrow-right"></span>':'')?></td>
      <td style="width:10px;"><?=(($item['offset']<3)?'<span class="glyphicon glyphicon-arrow-right"></span>':'')?></td>
      <td style="width:10px;"><?=(($item['offset']<4)?'<span class="glyphicon glyphicon-arrow-right"></span>':'')?></td>
	    <td<?=($item['offset'])?>></td>
      <td><?=$item['pid']?></td>
      <td><?=$item['title']?></td>
      <td><?=$item['name']?></td>
      <td><?=$item['created']?></td>
      <td><?=$item['changed']?></td>
    </tr>
  <?}?>
  </tbody>
</table>
<?}?>

