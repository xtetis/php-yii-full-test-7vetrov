<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Тестовое задание!</h1>
    </div>

    <div class="body-content">
<p>

1) Есть массив BB кодов в текстовом представлении

Преобразовать его в 2 PHP массива, где ключ массива – название BB кода, значение в одном массиве это данные BB кода, а в другом это описание BB кода

* Ограничение – вложенность BB кодов не допускается, т.е не надо это учитывать в обработке. Коды “размазаны” по тексту и не следуют друг за другом.

Описание BB кода начинается после символа “:”. Описание может отсутствовать. Закрывающий BB код – обязателен.
</p><p>
Формат BB код: [ BB :ОПИСАНИЕ]ДАННЫЕ[/ BB ]

Пример для проверки кода:
<pre>
980923ылвоащшываг902к3ыов

[resolve_urls]f1;ddd

mmm[/resolve_urls]ыва.юбьгш

[banned_urls]f1;ddd;mmm[/banned_urls][own_domain]true[/own_domain]

[ban_file]f1;ddd;mmm[/ban_file]

[ban_mime]f1;ddd;mmm[/ban_mime]

[iconv_file:WINDOWS-1251=>UTF-8]f1;ddd;mmm[/iconv_file]

[iconv_file:KOI8-R=>UTF-8]f1;ddd;mmm[/iconv_file]

[iconv_mime:WINDOWS-1251=>UTF-8]f1;

ddd;

mmm

[/iconv_mime]sd908f90

[replace_file]inF===ourF;ddd===ppp;mmm===rrr[/replace_file]

[add_file:FILE]DATA[/add_file]

dsiu90843

[post:URL_LOCATION]DATA

[/post]

,dfkdjfkdjf
</pre>
</p>







<p>2) Есть текстовые данные в следующем формате:</p>
<pre>
url: DATA1

get: DATA2

post: DATA3
</pre>

<p>
Эти данные необходимо преобразовать в PHP массив, где ключ это get , url , post , а значение это DA ТА. Последовательность ключей – любая. Как только встретился

хоть один ключ в тексте, то весь последующий текст необходимо преобразовывать в PHP массив:

Решение задачи должно быть выстроено на основе использование регулярных выражений( perl -совместимых)
</p>
<p>Пример для проверки:</p>
<pre>
dfkjsfkj dskljsdlfm sdfkjdsf sfsd get

asdkljasd,laosid post:

url: oiwerweroi;get:787wesdjhfsdfnxczp
</pre>











<p>3) Есть таблица в которой храниться дерево:</p>
<pre>
CREATE TABLE pages (

pid int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

title varchar(255) NOT NULL,

name varchar(255) default NULL,

parent int(10) UNSIGNED default NULL,

created int(10) NOT NULL,

changed int(10) NOT NULL,

PRIMARY KEY ( pid )) TYPE = MyISAM
</pre>
<p>Описание необходимых полей(остальные опускаем)</p>
<p>id - уникальный идентификатор узла дерева. Ограничения: Данное поле заполняется автоматически.</p>
<p>name - Название узла дерева. Ограничения: Данное поле не обязательно к заполнению.</p>

<p>parent - Идентификатор родительского узла(эта же таблица). Самый верхний узел имеет идентификатор NULL , т.е. тот который не имеет родителей. Ограничение: Данное поле не обязательно для заполнения и связано с полем id в этой же таблице.</p>

<p>Необходимо написать код который на основе это таблицы построет дерево страниц в виде:</p>
<pre>
UUU

->MM

->MM

UU

->N

->XX

->DD

->P

DDD

и т.д.
</pre>








<p>4) Выбрать из ране приведенной таблице все узлы которые не имеют родителей, но при этом содержать не менее 3 прямых потомков. (реализуется через SQL запрос)</p>






<p>5) Выбрать из ране приведенной таблице все узлы которые имеют только двух старших родителей, но при этом не имеют потомков. (реализуется через SQL запрос)</p>

<p>6) Найти повторяющееся числа в массиве с числами в диапазоне от 100 000 до 1 500 000. Количество повторяющихся чисел - 1-но, элементов в массиве - не меньше 1 000 000. Решить задачу на PHP минимальным использованием процессорного времени.</p>

<p>7) Удобно и наглядно представить результаты тестового задания, результат буду смотреть на последнем Open Server. Жду в реализации демострации навыков ООП и MVC</p>
    </div>
</div>
