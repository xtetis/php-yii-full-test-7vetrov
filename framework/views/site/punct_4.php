<?if (count($result)){?>
<table class="table table-bordered">
  <thead>
    <tr>
      <th>ID</th>
      <th>Title</th>
      <th>Name</th>
      <th>Created</th>
      <th>Changed</th>
    </tr>
  </thead>
  <tbody>
  <?foreach ($result as $item){?>
    <tr>
      <td><?=$item['pid']?></td>
      <td><?=$item['title']?></td>
      <td><?=$item['name']?></td>
      <td><?=$item['created']?></td>
      <td><?=$item['changed']?></td>
    </tr>
  <?}?>
  </tbody>
</table>
<?}?>

