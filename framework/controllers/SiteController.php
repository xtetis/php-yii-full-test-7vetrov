<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
	
    public function behaviors()
    {

        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
    
    
	public function actionPunct1()
	{
		$data=array();
		$result = array();
		$request = Yii::$app->request;
		if ($request->isPost)
		{
			$regex = "/\[([\w]+?)(:[\w|=|>|-]*?)?\]((.|\s)*)\[\/(\w+)\]/Uim";
			$postregex = Yii::$app->request->post('postregex');
			preg_match_all($regex,$postregex,$matches);
			
			for ($i = 0; $i < count($matches[0]); $i++)
			{
				$result[$matches[5][$i]]['bb_data'] = $matches[3][$i];
				$result[$matches[5][$i]]['bb_description'] = $matches[2][$i];
			}
			
		}
		$data['result'] = $result;
		
		return $this->render('punct_1',$data);
	}




	public function actionPunct2()
	{
		$data=array();
		$result = array();
		$request = Yii::$app->request;
		if ($request->isPost)
		{
			$regex = "/(get|post|url):(.*?)/Uim";
			$postregex = Yii::$app->request->post('postregex');
			$postregex = preg_replace("/(get|post|url):/Uim","\n".'${1}:',$postregex);
		//	echo $postregex; exit;
			preg_match_all($regex,$postregex,$matches);
			
			
			for ($i = 0; $i < count($matches[0]); $i++)
			{
				$result[] = array($matches[1][$i],$matches[2][$i]);
			}
//			$result = $matches;
			
		}
		$data['result'] = $result;
		
		return $this->render('punct_1',$data);
	}
	

	public function actionPunct3()
	{
		$sql = "SELECT * FROM `pages` WHERE `parent` IS NULL";
		$data = array();
		$data['result'] = array();
		$reader =\Yii::$app->db->createCommand($sql)->query(); 
		foreach ($reader as $row)
		{
			$row['offset']=0;
			$data['result'][] = $row;
			$sql = "SELECT * FROM `pages` WHERE `parent` =".$row['pid'];
			$reader1 =\Yii::$app->db->createCommand($sql)->query(); 
			foreach ($reader1 as $row1)
			{
				$row1['offset']=1;
				$data['result'][] = $row1;
				$sql = "SELECT * FROM `pages` WHERE `parent` =".$row1['pid'];
				$reader2 =\Yii::$app->db->createCommand($sql)->query(); 
				foreach ($reader2 as $row2)
				{
					$row2['offset']=2;
					$data['result'][] = $row2;
					$sql = "SELECT * FROM `pages` WHERE `parent` =".$row2['pid'];
					$reader3 =\Yii::$app->db->createCommand($sql)->query(); 
					foreach ($reader3 as $row3)
					{
						$row3['offset']=3;
						$data['result'][] = $row3;
					}
				}
			}
			
		}

		return $this->render('punct_3',$data);
	}
	
	
	

	public function actionPunct4()
	{
		$sql = "
SELECT
*
from
(

SELECT 
	t1.*,
	(select count(*) FROM `pages` t2 WHERE t2.`parent`=t1.`pid`) as 'count'
FROM `pages` t1
WHERE 
	`pid` in (SELECT `parent` FROM `pages`) 
	AND 
	`parent` IS NULL
) t3

WHERE
t3.count>=3
		";
		$data = array();
		$data['result'] = array();
		$reader =\Yii::$app->db->createCommand($sql)->query(); 
		foreach ($reader as $row)
		{
		$data['result'][] = $row;
		}

		return $this->render('punct_4',$data);
	}
	








	public function actionPunct5()
	{
		$sql = "
SELECT * FROM `pages` WHERE `pid` NOT IN 
(
SELECT `parent` FROM `pages` WHERE `parent` IS NOT NULL
)
AND
`parent` in
(
SELECT `pid` FROM `pages` WHERE `parent` in
(
SELECT `pid` FROM `pages` WHERE `parent` IS NULL
)
)

		";
		$data = array();
		$data['result'] = array();
		$reader =\Yii::$app->db->createCommand($sql)->query(); 
		foreach ($reader as $row)
		{
		$data['result'][] = $row;
		}

		return $this->render('punct_4',$data);
	}
	
	
	
	


	public function actionPunct6()
	{
//		$array1 = range(100000, 1500000);
//		$array2 = range(100000, 1500000);
//		$array3 = range(100000, 1500000);

		$array1 = range(1000, 15000);
		$array2 = range(1000, 15000);
		$array3 = range(1000, 15000);

		shuffle($array1);
		shuffle($array2);
		shuffle($array3);
		
//		$array_tmp1 = array_chunk($array1,500000);
//		$array_tmp2 = array_chunk($array2,500000);
//		$array_tmp3 = array_chunk($array3,500000);
		$array_tmp1 = array_chunk($array1,5000);
		$array_tmp2 = array_chunk($array2,5000);
		$array_tmp3 = array_chunk($array3,5000);

		
		$result_array = array_merge($array_tmp1[0],$array_tmp2[0],$array_tmp3[0]);
		$result_array = array_count_values($result_array);
		
		$result_array = array_filter($result_array,array( $this, 'gettwo'));

		file_put_contents($_SERVER['DOCUMENT_ROOT'].'/1.txt',print_r($result_array,true));
		return $this->render('punct_6');
	}
	

    private function gettwo($var)
		{
			return ($var == 2);
		}
	
	
}
