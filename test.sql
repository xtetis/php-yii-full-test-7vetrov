-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Сен 04 2015 г., 19:37
-- Версия сервера: 5.5.44-0ubuntu0.14.04.1
-- Версия PHP: 5.5.9-1ubuntu4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `test`
--

-- --------------------------------------------------------

--
-- Структура таблицы `pages`
--

DROP TABLE IF EXISTS `pages`;
CREATE TABLE IF NOT EXISTS `pages` (
  `pid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `parent` int(10) unsigned DEFAULT NULL,
  `created` int(11) NOT NULL,
  `changed` int(11) NOT NULL,
  PRIMARY KEY (`pid`),
  KEY `parent` (`parent`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Страницы' AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `pages`
--

INSERT INTO `pages` (`pid`, `title`, `name`, `parent`, `created`, `changed`) VALUES
(1, 'aaaaaa', 'aaaaaaa', NULL, 0, 0),
(2, 'bbbbbbbbbb', 'bbbbbbbbbb', 1, 1111111, 222222222),
(3, 'ccccccccccc', 'ccccccccccccc', NULL, 1111111, 222222222),
(4, 'ddddddddd', 'ddddddddddd', 2, 1111111, 222222222),
(5, 'ffffffffffff', 'fffffffffff', 4, 1111111, 222222222),
(6, 'gggggggggggggg', 'ggggggggggggg', 3, 1111111, 222222222),
(7, 'rrrrrrrrrr', 'rrrrrrrrr', 2, 1111111, 2147483647),
(8, 'yyyyyyyyyy', 'yyyyyyyy', 2, 444444, 4444444),
(9, 'wwwwwww', 'wwwwwww', 1, 222222, 2222222),
(10, '2222222', '2222222222', 1, 22222222, 222222222);

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `pages`
--
ALTER TABLE `pages`
  ADD CONSTRAINT `pages_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `pages` (`pid`) ON DELETE NO ACTION ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
